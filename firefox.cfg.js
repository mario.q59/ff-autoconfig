//
//
//  01 07-06-23

/********************************
 *  martedì, 9 novembre 2010
 * Questo file di configurazione viene richiamato dalle preferenze
 *   pref("general.config.obscure_value", 0);
 *   pref("general.config.filename", "firefox.cfg");
 * aggiunte alla fine del file "/usr/lib/firefox-<n ver>/greprefs/all.js"
 * lo stesso file anche per firefox su windows
 *
 * Il file firefox.cfg deve trovarsi alla radice della cartella di
 * firefox in questo esempio "/usr/lib/firefox-<n ver>/" e deve contenere
 * come minimo queste inpostazioni:
 *   lockPref("autoadmin.global_config_url","http://myDommain.qq/firefox.cfg.js");
 *   lockPref("autoadmin.append_emailaddr",false);
 * vedi:
 * https://bug222973.bugzilla.mozilla.org/attachment.cgi?id=152002#p00054
 * http://mit.edu/~firefox/www/maintainers/autoconfig.html
 * http://www.alain.knaff.lu/howto/MozillaCustomization/
 * https://developer.mozilla.org/en/MCD,_Mission_Control_Desktop_AKA_AutoConfig
 * https://developer.mozilla.org/en/Places
 *
 * http://www.google.it/#hl=it&source=hp&biw=1198&bih=727&q=firefox+autoconfig&btnG=Cerca+con+Google&aq=f&aqi=&aql=&oq=firefox+autoconfig&gs_rfai=&fp=7186b3871a8c2c9e
 * https://developer.mozilla.org/en/docs/MCD,_Mission_Control_Desktop_AKA_AutoConfig
 *
 * Questo file, così com'è, imposta firefox per l'uso su computer pubblici.
 * le impostazioni principali sono:
 * non tiene traccia delle sessioni, dei cookie, della cronologia, delle passwords, dei downloads.
 * Alla chiusura di firefox elimina tutte le tracce della navigazione.
 *
 * Tutte le impostazioni delle preferenze (ed altro, ma non tutto) sono state impostate con l'obbiettivo di una
 * navigazione anonima avanzata, senza togliere la possibilità di poche personalizzazioni.
 * ad esempio, la personalizzazione della lingua, dei caratteri e della loro codifica,
 * la cronologia dei downloads, la personalizzazione degli stili, e poche altre.
 *
 * Al riavvio di firefox tutto viene reimpostato con i valori qui specificati.
 * penso di aver messo tutto il necessario, semmai aggiorno.
 * firefox.cfg.js v0.0 martedì, 9 novembre 2010
 * ciao Mario
 * 
 * Da firefox71 about:config viene visualizzato con un'altra interfaccia.
 * per vedere la precedente interfaccia 
 * chrome://global/content/config.xhtml
 * oppure
 * chrome://global/content/config.xul
 *****************************************/

/**
 * https://developer.mozilla.org/en-US/Firefox/Enterprise_deployment
 * https://developer.mozilla.org/en-US/Firefox/Enterprise_deployment_before_60#Configuration
 * https://support.mozilla.org/it/products/firefox-enterprise
 * https://support.mozilla.org/it/kb/customizing-firefox-using-autoconfig
 * https://developer.mozilla.org/en-US/docs/Mozilla/Preferences/A_brief_guide_to_Mozilla_preferences
**/

/**
 * 21.01.2016 12:08:19
 * /usr/lib/firefox/defaults/pref/mario-autoconfig.js
 * ------------------
// mario
pref("general.config.obscure_value", 0);
pref("general.config.filename", "mario-firefox.cfg");

* --------------------
*
* /usr/lib/firefox/mario-firefox.cfg
* ---------------------
// mario
lockPref("autoadmin.global_config_url","http://mio_dominio_in_lan/cfg/ff/firefox.cfg.js");
lockPref("autoadmin.append_emailaddr",false);

* ---------------------
**/

/**
userChrome.css nella cartella "profilo/chrome" nasconde alcuni comandi

@namespace url("http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul"); // * set default namespace to XUL
	#context-blockimage,
	#context-setDesktopBackground,
	#context-sendpage,
	#context-sendlink,
	#context-sendimage {
	display: none !important ;
	}
**/

/**  per le funzioni diponibili vedi prefcalls.js
 *  URL "jar:file:///usr/lib/firefox/omni.ja!/defaults/autoconfig/prefcalls.js"
 *  function
 *	pref(prefName, value)
 *	defaultPref(prefName, value)
 *	clearPref(prefName)
 *	lockPref(prefName, value)
 *	unlockPref(prefName)
 *	getPref(prefName)
 *	displayError(funcname, message)
 *	getenv(name)
 *	getLDAPValue(str, key)
 *	getLDAPAttributes(host, base, filter, attribs)
**/

/**
 * http://kb.mozillazine.org/About:config_entries
 * configurazione predefinita
 * "resource:///defaults/preferences/firefox.js"
**/

/**
 * https://wiki.mozilla.org/Firefox/Roadmap
**/

// try {

/**
 * imposto una mia preferenza
 * viene valorizzata da windows con il valore stringa "Windows_NT"
**/
pref("my.system.os", getenv("MYFOLDER_IMG_BG"));
/* clearPref("my.system.os"); */

if(getenv("OS")=="Windows_NT"){
	var OS="Windows_NT" ;
}


pref("my.env.LOGNAME", getenv("LOGNAME"));

if(getenv("SHELL")===""){ //windows non ha SHELL
	pref("my.env.SHELL", "noSHELL");
}else{
	pref("my.env.SHELL", getenv("SHELL"));
	// pref("my.env.SHELL", getenv("MYSCRIPTS"));
}


////////////////////////////
/**

**/
pref("my.CURRENT_HOST_NAME","no_name");
pref("my.CURRENT_HOST_OS","no_name");

let CURRENT_HOST_NAME="";
let CURRENT_HOST_OS="";
if(getenv("COMPUTERNAME")!==""){
	CURRENT_HOST_NAME=getenv("COMPUTERNAME").toLowerCase();
	pref("my.CURRENT_HOST_NAME", CURRENT_HOST_NAME);
	if(getenv("OS")!==""){
		CURRENT_HOST_OS=getenv("OS");	
		pref("my.CURRENT_HOST_OS", CURRENT_HOST_OS);
	}
}

if(getenv("SHELL")!==""){ //windows usa COMPUTERNAME
	let CURRENT_HOST_OS=getenv("SHELL");
	pref("my.CURRENT_HOST_OS", "is_linux"+CURRENT_HOST_OS); // forse /bin/bash
}
////////////////////////////


pref("autoadmin.refresh_interval", 42); //minuti

/**
 * Don't show 'know your rights' on first run
 * pref("browser.rights.3.shown", true);
 *
 * Don't show WhatsNew on first run after every update
 * pref("browser.startup.homepage_override.mstone","ignore");
 *
**/

/**
 * Which part of the preferences window was displayed the last time it was closed. In Firefox:
 * 0: General
 * 1: Privacy
 * 2: Web Features
 * 3: Downloads
 * 4: Advanced
 * Note: In Firefox, this is determined by the section of
 * "Tools → Options" you are viewing when you close the dialog.
 * pref("browser.preferences.lastpanel", 0); // nelle nuove versioni non viene considerata
**/

pref("browser.slowStartup.notificationDisabled", true);

// sessioni inizio ////////////////////////////////////////////////////
/** impostate per non tenere traccia delle sessioni
 * http://kb.mozillazine.org/Session_Restore  **/

	pref("browser.sessionstore.enabled", false);
	pref("browser.sessionstore.resume_from_crash", false);
	pref("browser.sessionstore.max_tabs_undo", 5);
	pref("browser.sessionstore.max_windows_undo", 0);
// sessioni fine  ////////////////////////////////////////////////////

	// impostata più in basso pref("browser.defaultbrowser.notificationbar", true);
	pref("browser.bookmarks.max_backups", 0);
	pref("lightweightThemes.usedThemes", "[]");
	pref("general.skins.selectedSkin", "classic/1.0");

	pref("browser.theme.toolbar-theme", 2);
	pref("browser.theme.content-theme", 2);
	pref("extensions.activeThemeID", "default-theme@mozilla.org");
	pref("layout.css.prefers-color-scheme.content-override", 2);
	// (0 - dark, 1 - light, 2 - system, 3 - browser)
	
	pref("browser.tabs.inTitlebar", 0);
	pref("browser.uidensity", 0);
	/**
	 * browser.uidensity
	 * 0 normale
	 * 1 compatta
	 * 2 thouch
	**/
	pref("browser.toolbars.bookmarks.visibility", "newtab"); // always newtab never
	pref("browser.display.document_color_use", 1);	
	pref("layout.css.prefers-color-scheme.content-override", 2);
	/**
	 * 0 NERO
	 * 1 CHIARO
	 * 2 Thema di sistema
	 * 3 Thema di Firefox
	 **/

	pref("general.warnOnAboutConfig", true);

////// analisi integrità
	pref("datareporting.healthreport.uploadEnabled", true);
// //safebrowsing predefinito con google
	pref("browser.safebrowsing.enabled", true);
	pref("browser.safebrowsing.malware.enabled", true);

//inizio preferenze GENERALE
	//clearPref("browser.startup.homepage");
	pref("browser.startup.page", 1);

	//SEARCH_THIS="";
	SEARCH_THIS="ong ProActiva open arms";
	// https://start.duckduckgo.com/params
	/**Barra indirizzi: kg = g per GET; p per POST.**/
	DUCK_PARAMS="&kg=g";
	DUCK_PARAMS+="&kl=it-it"; //zona
	/**Open Instant Answers: kz = 1 per On; -1 per Off. **/
	DUCK_PARAMS+="&kz=1";
	/**Pubblicità: k1=1 per On (predefinito) . k1=-1 Off**/
	DUCK_PARAMS+="&k1=-1";
	// solo uno
	//DUCK_PARAMS+="&ia=images&iaf=size%3Aimagesize-large"; // wallpaper ricerca Immaggini Molto Largo
	//DUCK_PARAMS+="&ia=web"; // ricerca nel web
	//DUCK_PARAMS+="&ia=videos"; // ricerca Video
	DUCK_PARAMS+="&ia=news"; // ricerca news ??
	/**Pagina #s - 1 per On - n per On, ma nessun numero - -1 per Off (predefinito) **/
	DUCK_PARAMS+="&kv=1";
	/**Directions Source:
    apple-maps per Apple Maps
    bing-maps per Bing Maps
    google-maps per Google Maps
    here-maps per HERE Maps
    osm per OpenStreetMap **/
	DUCK_PARAMS+="$kam=osm";
	DUCKDUCKGO_URL="https://start.duckduckgo.com/?q="+SEARCH_THIS+DUCK_PARAMS+"";
	// DUCKDUCKGO_URL="https://duckduckgo.com/?"+DUCK_PARAMS+"";

	MONTH=["gennaio","febbraio","marzo","aprile","maggio","giugno","luglio","agosto","settembre","ottobre","novembre","dicembre"];
	DATE = new Date();
	DAY_MONTH = DATE.getDate()+"_"+MONTH[DATE.getMonth()];
	WIKI_URL="https://it.wikipedia.org/wiki/"+DAY_MONTH;

	//let FF_HOME_PAGE="https://publiccode.eu/it/|";
	//let FF_HOME_PAGE="https://bdsitalia.org/index.php/la-campagna-bds/ultime-notizie-bds|";
	//let FF_HOME_PAGE="https://www.occhionotizie.it/wp-content/uploads/2019/10/palestina.jpg|";
	let FF_HOME_PAGE="https://zic.it/wp-content/uploads/LPN4527.jpg|";
	/************************
	if (CURRENT_HOST_NAME==="acer1431"){ //INGRESSO
		FF_HOME_PAGE="https://www.istruzione.it/iscrizionionline|";
	}
	if (CURRENT_HOST_NAME==="arco904"){
		FF_HOME_PAGE="https://mastodon.bida.im/tags/bicicletta|";
	}
	if (CURRENT_HOST_NAME.match(/hp[^a-z].*[x]?/ig)) {
		FF_HOME_PAGE="https://benzinazero.wordpress.com/2023|https://mastodon.bida.im/tags/bicicletta|";
	}
	if (CURRENT_HOST_NAME==="hp1864"){
		FF_HOME_PAGE="https://benzinazero.wordpress.com/2023|";
	}

	if (CURRENT_HOST_NAME.match(/elm[0-9]./i)) {
		FF_HOME_PAGE="https://ms13.invalsi.taocloud.org|";
	}
	if (CURRENT_HOST_NAME.match(/luna[0-9]./i)) {
		FF_HOME_PAGE="https://ms13.invalsi.taocloud.org|";
	}
	if (CURRENT_HOST_NAME.match(/arco[0-9]./i)) {
		FF_HOME_PAGE="https://ms13.invalsi.taocloud.org|";
	}
	************************/

	pref("browser.startup.homepage", FF_HOME_PAGE+WIKI_URL+"|about:newtab");
	//pref("browser.startup.homepage", "about:robots|about:debugging|about:telemetry|about:config");
	//pref("browser.startup.homepage", "about:debugging");
	//pref("browser.startup.homepage", ""+DUCKDUCKGO_URL+"|"+WIKI_URL+"|about:newtab");
	//pref("browser.startup.homepage", "https://www.openarms.es/it|"+DUCKDUCKGO_URL+"|"+WIKI_URL);
	//pref("browser.startup.homepage", "https://sea-watch.org/it/|"+DUCKDUCKGO_URL+"|"+WIKI_URL);
	//pref("browser.startup.homepage", "about:home|"+WIKI_URL);
	//pref("browser.startup.homepage", "about:newtab|"+DUCKDUCKGO_URL+"|"+WIKI_URL);
	//pref("browser.startup.homepage", "about:home");
	//pref("browser.startup.homepage", "about:home|"+WIKI_URL+"|"+DUCKDUCKGO_URL);
	//pref("browser.startup.homepage", "https://ms10.invalsi.taocloud.org/|about:newtab|"+WIKI_URL);
	

	pref("pref.browser.homepage.disable_button.current_page", true);

	pref("pref.browser.homepage.disable_button.restore_default", true);

	pref("pref.browser.homepage.disable_button.bookmark_page", true);

	pref("browser.download.manager.showWhenStarting", true);

	pref("browser.download.manager.closeWhenDone", false);

	pref("browser.download.useDownloadDir", true);
	pref("browser.download.dir", "~/Scaricati");
	pref("browser.download.lastDir", "~/Scaricati");

	pref("browser.download.manager.scanWhenDone", true);

	pref("browser.download.folderList", 1);

	// //https://developer.mozilla.org/en/Download_Manager_preferences
	pref("browser.helperApps.deleteTempFileOnExit", true);

	pref("browser.download.manager.alertOnEXEOpen", true);

	pref("browser.download.autohideButton", true);
//fine preferenze GENERALE

//inizio preferenze SCHEDE
//http://kb.mozillazine.org/Browser.link.open_newwindow.restriction
	pref("browser.link.open_newwindow", 3);

	// When tabs opened by links in other tabs via a combination of
	// browser.link.open_newwindow being set to 3 and target="_blank" etc are
	// closed:
	// true   return to the tab that opened this tab (its owner)
	// false  return to the adjacent tab (old default)
	pref("browser.tabs.selectOwnerOnClose", true);

	// new in firefox 60
	pref("browser.tabs.drawInTitlebar", false);
	pref("browser.tabs.extraDragSpace", true);

	pref("browser.tabs.warnOnCloseOtherTabs", false);
	pref("browser.tabs.warnOnClose", false);
	pref("browser.tabs.warnOnOpen", false);
	pref("browser.tabs.autoHide", true);
	// //unlockPref("browser.tabs.loadInBackground");
	// lockPref("browser.tabs.loadInBackground", true);
	pref("browser.ctrlTab.previews", true);
	pref("browser.allTabs.previews", true);
	
	// 0 finestra con barra del titolo
	// 1 finestra senza barra del titolo
	pref("browser.tabs.inTitlebar", 0);

// è stata disabilitata da firefox 41
	//pref("browser.newtab.url", "https://start.duckduckgo.com");
	pref("browser.newtab.url", "https://www.wikimedia.org/disabilitata da firefox 41 ");
	// Activates preloading of the new tab url.
	pref("browser.newtab.preload", true); //??

// sostituisce about:home con about:newtab
	pref("browser.newtabpage.enabled", true);

// dalla versione 130
//color
	//myWallpaper="blue";
	//myWallpaper="red";
	//myWallpaper="orange";
//image
	//myWallpaper="dark-beach";
	//myWallpaper="dark-sky";
//abstract
	myWallpaper="light-fox-anniversary";
	//myWallpaper="abstract-orange";
	//myWallpaper="light-color";
	//myWallpaper="abstract-blue";

	pref("browser.newtabpage.activity-stream.newtabWallpapers.v2.enabled", true);
	pref("browser.newtabpage.activity-stream.newtabWallpapers.enabled", true);

	//pref("browser.newtabpage.activity-stream.newtabWallpapers.highlightDismissed", true); 
	pref("browser.newtabpage.activity-stream.newtabWallpapers.wallpaper", myWallpaper);
	pref("browser.newtabpage.activity-stream.newtabWallpapers.wallpaper-dark", myWallpaper);
	pref("browser.newtabpage.activity-stream.newtabWallpapers.wallpaper-light", myWallpaper);


// newtabpage firefox 57 - 58
	//false visualizza importa da altri browser
	pref("browser.newtabpage.activity-stream.sectionOrder", "topsites,highlights,topstories");
	pref("browser.newtabpage.activity-stream.migrationExpired", true);
	pref("browser.newtabpage.enhanced", true);
	pref("browser.newtabpage.activity-stream.prerender", true);

	pref("browser.newtabpage.activity-stream.feeds.section.highlights", true); // in evidenza
	pref("browser.newtabpage.activity-stream.section.highlights.collapsed", true);

	/** dalla version 63 prende il valore qui sotto **/
	pref("browser.newtabpage.activity-stream.feeds.snippets", false);
	pref("browser.newtabpage.activity-stream.disableSnippets", true);

	// enables showing basic placeholders for missing thumbnails
	pref("browser.newtabpage.thumbnailPlaceholder", true); //??

	pref("browser.newtabpage.activity-stream.showSearch", true);

	////////
	//pref("browser.newtabpage.activity-stream.showTopSites", true);
	// dalla versione 61 prende il valore qui sotto
	pref("browser.newtabpage.activity-stream.feeds.topsites", true);
	pref("browser.newtabpage.activity-stream.topSitesRows", 6); // Il valore massiomo via GUI è 4
	///////
	pref("browser.newtabpage.activity-stream.collapseTopSites", true);

	pref("browser.newtabpage.compact", false);
	pref("browser.newtabpage.activity-stream.enabled", true); //
	//pref("browser.newtabpage.rows", 5); //default 3
	//pref("browser.newtabpage.columns", 8); //default 5

	//myTopSitesCount=Math.floor(getPref("browser.newtabpage.activity-stream.topSitesRows") * 8);
	myTopSitesCount=41;
	pref("browser.newtabpage.activity-stream.topSitesCount", myTopSitesCount ); //default 12 due righe
	/** è il prodotto di
	 * (newtabpage.columns * newtabpage.rows)=newtabpage.activity-stream.topSitesCount
	 * solo se pref("browser.newtabpage.activity-stream.enabled", false);
	**/
	pref("browser.newtabpage.activity-stream.feeds.section.topstories", false); //pocket
	pref("browser.newtabpage.activity-stream.section.topstories.collapsed", true);
	pref("browser.newtabpage.activity-stream.section.topstories.showDisclaimer", true);
	pref("browser.newtabpage.activity-stream.section.topstories.rows", 6); // from gui max 4
	pref("browser.newtabpage.activity-stream.showSponsored", false);
	pref("browser.newtabpage.activity-stream.showSponsoredTopSites", false);
	// pref("browser.newtabpage.activity-stream.feeds.section.topstories.options",'{"api_key_pref":"extensions.pocket.oAuthConsumerKey","hidden":false,"provider_header":"pocket_feedback_header","provider_description":"pocket_description","provider_icon":"pocket","provider_name":"Pocket","read_more_endpoint":"https://getpocket.com/explore/italy?src=fx_new_tab","stories_endpoint":"https://getpocket.cdn.mozilla.net/v3/firefox/global-recs?version=2&consumer_key=$apiKey&locale_lang=it-IT","stories_referrer":"http://getpocket.com/recommendations","info_link":"https://www.mozilla.org/privacy/firefox/#pocketstories","disclaimer_link":"https://getpocket.cdn.mozilla.net/firefox/new_tab_learn_more","topics_endpoint":"https://getpocket.cdn.mozilla.net/v3/firefox/trending-topics?version=2&consumer_key=$apiKey&locale_lang=it-IT","show_spocs":true,"personalized":true}');

/*************************************************************************/
	myWWW=[]; // myWWW[??] = {"show":"false","url":"https://myurl","label":"mylabel","customScreenshotURL":"no"};

	// myWWW[0] = {"show":"false","url":"https://www.letour.fr/en/","label":"Tour de France","customScreenshotURL":"https://www.letour.fr/apple-touch-icon.png"};

	myWWW[0] = {"show":"true","url":"https://ms10.invalsi.taocloud.org/","label":"10-TEST-INVALSI","customScreenshotURL":"https://static.itinv.taocloud.org/tao/diagnostic/v0.0.1/taoInvalsi/views/img/themes/platform/taoInvalsiDefaultTheme/logo-invalsi.jpg"};

	myWWW[1] = {"show":"true","url":"https://ms13.invalsi.taocloud.org/","label":"13-TEST-INVALSI","customScreenshotURL":"https://static.itinv.taocloud.org/tao/diagnostic/v0.0.1/taoInvalsi/views/img/themes/platform/taoInvalsiDefaultTheme/logo-invalsi.jpg"};
	
	myWWW[2] = {"show":"false","url":"https://publiccode.eu/it/","label":"FreeSoftware","customScreenshotURL":"https://publiccode.eu/favicon.png"};
	myWWW[3] = {"show":"false","url":"https://iam.pubblica.istruzione.it/","label":"Commissione web","customScreenshotURL":"https://commissioni.esamidistato.istruzione.it/static_home/images/Logo_Commissioni_Web_03.svg"};

	//myWWW[4] = {"show":"true","url":"https://codeberg.org/marioq59","label":"git-CODEBERG","customScreenshotURL":""};
/***
	myWWW[4] = {"show":"false","url":"......","label":"......","customScreenshotURL":""};
	myWWW[5] = {"show":"false","url":"......","label":"......","customScreenshotURL":""};
	myWWW[6] = {"show":"false","url":"......","label":"......","customScreenshotURL":""};
	myWWW[7] = {"show":"false","url":"......","label":"......","customScreenshotURL":""};
	myWWW[8] = {"show":"false","url":"......","label":"......","customScreenshotURL":""};
	myWWW[9] = {"show":"false","url":"......","label":"......","customScreenshotURL":""};
***/

	myWWW[10] = {"show":"false","url":"https://pippo.com/","label":"PIPPO","customScreenshotURL":"no"};

	myWWW[11] = {"show":"true","url":"https://www.iispareto.it/","label":"iisPARETO","customScreenshotURL":"https://www.iispareto.it/wp-content/uploads/2017/08/cropped-logo-scuola-icona-def-2-3-180x180.jpg"};

	myWWW[12] = {"show":"true","url":"https://europa.eu/european-union/index_it","label":"EUROPA","customScreenshotURL":"https://publications.europa.eu/code/images/scan/5000200-flag-cl.jpg"};

	myWWW[13] = {"show":"false","url":"","label":"","customScreenshotURL":""};

	myWWW[14] = {"show":"false","url":"http://www.cultura.rai.it","label":"RAICultura","customScreenshotURL":"NO_http://www.cultura.rai.it/images/network-italiano.jpg"};
	myWWW[15] = {"show":"true","url":"https://www.rai.it/raistoria","label":"RAIStoria","customScreenshotURL":"NO"};
	myWWW[16] = {"show":"false","url":"https://www.rai.it/raiscuola","label":"RAIScuola","customScreenshotURL":"NO"};
	myWWW[17] = {"show":"true","url":"https://www.rainews.it/archivio/sport?activeSubCategoryPipe=Ciclismo%7CCategory-337deaab-e725-4cd9-bc5e-806f37cee18f","label":"RAInewsCICLISMO","customScreenshotURL":""};


	myWWW[20] = {"show":"true","url":"https://it.wikiversity.org/wiki/Agronomia","label":"Agronomia","customScreenshotURL":"https://it.wikiversity.org/static/images/project-logos/itwikiversity.png"};


	myWWW[21] = {"show":"true","url":"https://www.geogebra.org/","label":"geogebra","customScreenshotURL":"https://wiki.geogebra.org/uploads/thumb/0/03/Geogebra-logo.svg/480px-Geogebra-logo.svg.png"};

	myWWW[22] = {"show":"true","url":"https://www.easyphp.org/","label":"EasyPHP","customScreenshotURL":"https://www.easyphp.org/libraries/img/logo_easyphp_100x100.png"};


	myWWW[23] = {"show":"true","url":"https://portableapps.com/apps","label":"appsPortabili","customScreenshotURL":"https://portableapps.com/apple-touch-icon.png"};

	myWWW[24] = {"show":"false","url":"https://www.bsmart.it/users/sign_in","label":"MyBSmart","customScreenshotURL":""};

	myWWW[25] = {"show":"false","url":"","label":"","customScreenshotURL":""};

	myWWW[26] = {"show":"true","url":"https://www.mozilla.org/it/privacy/firefox","label":"Privacy"};


	myWWW[27] = {"show":"true","url":"https://videocitofono.bida.im/","label":"BIDA conferenze"};
	myWWW[28] = {"show":"true","url":"https://meet.jit.si/","label":"jitsi conferenze"};



	myWWW[30] = {"show":"false","url":"https://www.mozilla.org/it/teach/smarton/surveillance/#ask","label":"Ti Spio??","customScreenshotURL":"no_show_https://www.mozilla.org/media/img/pebbles/moz-wordmark-dark-reverse.2cbc28bb9895.svg"};

	myWWW[31] = {"show":"true","url":"https://www.ilmeteo.it/meteo/Milano/previsioni-orarie","label":"IlMeteo.it","customScreenshotURL":"NO_https://www.ilmeteo.it/cartine3/0.LOM.png"};

/***

***/

	myWWW[33] = {"show":"true","url":"https://www.radiondadurto.org/","label":"RadioOndaD'Urto","customScreenshotURL":"NO_https://www.radiondadurto.org/wp-content/uploads/2019/04/LOGO_RADIO_GATTO-NERO-150x150.png"};

	myWWW[34] = {"show":"true","url":"https://www.radiopopolare.it/","label":"RPop","customScreenshotURL":"NO_https://www.radiopopolare.it/wp-content/uploads/2015/09/LogoGr-210x210.jpg"};

	myWWW[35] = {"show":"true","url":"https://mediaportal.regione.lombardia.it/embed/live/24","label":"2°cam-FalcoPellegrino","customScreenshotURL":"http://mediaportal.regione.lombardia.it/cms/vodgroups/snapshots/24/20200218_110813.png"};

/***

***/

	myWWW[40] = {"show":"true","url":"https://www.lifegate.it/radio-sound","label":"LIFEGATEradio","customScreenshotURL":""};

	myWWW[41] = {"show":"true","url":"https://www.ubuntu-it.org/progetto","label":"UBUNTI-it","customScreenshotURL":""};

	myWWW[42] = {"show":"true","url":"https://www.sellarondabikeday.com/","label":"SellaRondaBikeDay","customScreenshotURL":"https://www.sellarondabikeday.com/img/icona.png"};

	myWWW[43] = {"show":"true","url":"https://www.dolomitesbikeday.it/it/","label":"DolomitiBikeDay","customScreenshotURL":"https://www.dolomitesbikeday.it/images/favicon/apple-touch-icon.png"};
/***	
	myWWW[44] = {"show":"false","url":"......","label":"......","customScreenshotURL":""};
	myWWW[45] = {"show":"false","url":"......","label":"......","customScreenshotURL":""};
	myWWW[46] = {"show":"false","url":"......","label":"......","customScreenshotURL":""};
***/

	myWWW[47] = {"show":"true","url":"https://www.subito.it/annunci-lombardia/vendita/biciclette/milano/?sp=1&q=bici+corsa&o=3","label":"SUBITObici","customScreenshotURL":"no_show https://s.sbito.it/1201541689694/img/apple_touch_icon_180.png"};

	myWWW[48] = {"show":"true","url":"https://www.decathlon.it/C-1169036-ciclismo-su-strada","label":"decathlon.it","customScreenshotURL":""};

	myWWW[49] = {"show":"true","url":"https://www.sardicicli.com/","label":"SARDIbici"};

	myWWW[50] = {"show":"true","url":"https://www.chainreactioncycles.com/it/it/","label":"chainreactioncycles"};

	myWWW[51] = {"show":"true","url":"https://www.ridewill.it/h/it/ciclismo/1/","label":"riderwill"};

	myWWW[52] = {"show":"false","url":"http://www.fiab-onlus.it/bici/","label":"FIAB-bici"};

	myWWW[53] = {"show":"false","url":"http://www.fiab-areatecnica.it/","label":"FIAB-tec"};

	myWWW[54] = {"show":"false","url":"https://www.biciviaggi.it/","label":"bici-viaggi"};

	myWWW[55] = {"show":"true","url":"https://www.lordgunbicycles.com/","label":"Lordgun","customScreenshotURL":""};

	myWWW[56] = {"show":"true","url":"https://www.rosebikes.it/","label":"roseBike","customScreenshotURL":""};

	myWWW[57] = {"show":"true","url":"https://www.probikeshop.it/strada/","label":"probike","customScreenshotURL":""};

	myWWW[58] = {"show":"true","url":"https://www.bike24.com/","label":"BIKE24","customScreenshotURL":""};
/***
	myWWW[59] = {"show":"false","url":"......","label":"......","customScreenshotURL":""};
	myWWW[60] = {"show":"false","url":"......","label":"......","customScreenshotURL":""};

	myWWW[61] = {"show":"false","url":"......","label":"......","customScreenshotURL":""};
	myWWW[62] = {"show":"false","url":"......","label":"......","customScreenshotURL":""};
	myWWW[63] = {"show":"false","url":"......","label":"......","customScreenshotURL":""};
	myWWW[64] = {"show":"false","url":"......","label":"......","customScreenshotURL":""};
	myWWW[65] = {"show":"false","url":"......","label":"......","customScreenshotURL":""};

	myWWW[66] = {"show":"false","url":"......","label":"......","customScreenshotURL":""};
	myWWW[67] = {"show":"false","url":"......","label":"......","customScreenshotURL":""};
	myWWW[68] = {"show":"false","url":"......","label":"......","customScreenshotURL":""};
	myWWW[69] = {"show":"false","url":"......","label":"......","customScreenshotURL":""};
	myWWW[70] = {"show":"false","url":"......","label":"......","customScreenshotURL":""};

	myWWW[71] = {"show":"false","url":"......","label":"......","customScreenshotURL":""};
	myWWW[72] = {"show":"false","url":"......","label":"......","customScreenshotURL":""};
	myWWW[73] = {"show":"false","url":"......","label":"......","customScreenshotURL":""};
	myWWW[74] = {"show":"false","url":"......","label":"......","customScreenshotURL":""};
	myWWW[75] = {"show":"false","url":"......","label":"......","customScreenshotURL":""};

	myWWW[76] = {"show":"false","url":"......","label":"......","customScreenshotURL":""};
	myWWW[77] = {"show":"false","url":"......","label":"......","customScreenshotURL":""};
	myWWW[78] = {"show":"false","url":"......","label":"......","customScreenshotURL":""};
	myWWW[79] = {"show":"false","url":"......","label":"......","customScreenshotURL":""};
	myWWW[80] = {"show":"false","url":"......","label":"......","customScreenshotURL":""};
***/

	myWWW[90] = {"show":"true","url":"https://noipa.mef.gov.it","label":"NoiPA"};

	myWWW[91] = {"show":"true","url":"http://www.flcgil.it","label":"CGILscuola"};

	myWWW[92] = {"show":"true","url":"https://uilscuola.it/","label":"UILscuola"};

	myWWW[93] = {"show":"true","url":"https://cub.it/?s=scuola","label":"CUBscuola"};

	//myWWW[94] = {"show":"false","url":"https://developer.mozilla.org/it/docs/Mozilla/Preferences","label":"MOZDev"};

	myWWW[94] = {"show":"true","url":"https://codeberg.org/marioq59","label":"CODEBERG.org","customScreenshotURL":"https://codeberg.org/assets/img/favicon.png"};

	myWWW[95] = {"show":"true","url":"https://pastebin.com/u/marioq","label":"pasteMario","customScreenshotURL":"https://pastebin.com/cache/img/1/8/19/2080908.jpg"};

	myWWW[96] = {"show":"true","url":"https://gitlab.com/mario.q59","label":"GitLab-mario","customScreenshotURL":"https://about.gitlab.com/nuxt-images/ico/apple-touch-icon-76x76.png"};

	myWWW[97] = {"show":"true","url":"https://librespeed.org/","label":"speedtest","customScreenshotURL":"https://librespeed.org/favicon.ico"};

	myWWW[98] = {"show":"false","url":"https://bida.im/","label":"bida.im","customScreenshotURL":""};


	myWWW[99] = {"show":"true","url":"https://mastodon.bida.im/tags/bici","label":"mastodon.bida.im","customScreenshotURL":""};

	myWWW[100] = {"show":"true","url":"https://www.strava.com/athletes/marioq59","label":"STRAVA","customScreenshotURL":""};

	myWWW[101] = {"show":"true","url":"https://www.komoot.com/it-it/user/2714263956386","label":"Komoot"};

	// myWWW[102] = {"show":"false","url":"https://www.reddit.com","label":"reddit"};
	// myWWW[103] = {"show":"false","url":"https://www.amazon.it/","label":"amazon-it"};
	// myWWW[104] = {"show":"false","url":"https://www.ebay.it/","label":"EBay-it"};
	// myWWW[105] = {"show":"false","url":"https://twitter.com/?lang=it","label":"twuit-it"};

	myWWW[106] = {"show":"false","url":"https://accounts.google.com/signin/v2/identifier?service=mail","label":"Gmail","customScreenshotURL":"https://ssl.gstatic.com/ui/v1/icons/mail/rfr/gmail.ico"};
	myWWW[107] = {"show":"false","url":"https://accounts.google.com/signin/v2/identifier?service=wise","label":"Gdrive"};
	myWWW[108] = {"show":"true","url":"https://login.virgilio.it/", "label":"VIRGILIOmail.it"};
	myWWW[109] = {"show":"true","url":"https://login.libero.it/", "label":"LIBEROmail.it"};
	myWWW[110] = {"show":"false","url":"http://pfs.lanpareto:8443","label":"lanpareto"};

	myWWW[111] = {"show":"false","url":"http://googleadservices.com/","label":"tracker-403 forbidden","customScreenshotURL":""};

	myWWW[112] = {"show":"true","url":"https://pippo.com","label":"pippo.com","customScreenshotURL":""};

/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////

	pref("my.myWWWlength", myWWW.length);

	// myWWW.sort(function(a, b){return a-b}); // reverse
	myWWW.sort();

	//nLimit=25;
	//nLimit=getPref("browser.newtabpage.columns")+(Math.floor(getPref("browser.newtabpage.columns")/2));
	//nLimit=Math.floor(getPref("browser.newtabpage.activity-stream.topSitesCount")/2)-Math.floor(getPref("browser.newtabpage.columns")/2);
	//nLimit=Math.floor(getPref("browser.newtabpage.activity-stream.topSitesCount")/2);
	//nLimit=Math.floor(getPref("browser.newtabpage.activity-stream.topSitesCount")-6);
	//nLimit=Math.floor(myTopSitesCount-5);
	nLimit=myTopSitesCount;

	//nLimit=myWWW.length;
	/** no non può andare
	* nLimit impone il numero dei siti "appuntati - pinned"
	* deve essere inferione a "browser.newtabpage.activity-stream.topSitesCount"
	**/

	myWWWList="";
	count_myWWWList=0;
	for ( i=0 ; i<myWWW.length ; i++ ){
		if ( myWWW[i] != undefined && myWWW[i]['show'] === "true" ){
			count_myWWWList++;
			nLimit-- ;
			if ( nLimit >=0  ){
				endStr=',';
				if ( nLimit==0 ){
					endStr='';
				}

				if ( myWWW[i]['customScreenshotURL'] != undefined  ){
					if ( myWWW[i]['customScreenshotURL'].startsWith('http://') ||
					 myWWW[i]['customScreenshotURL'].startsWith('https://') ||
					 myWWW[i]['customScreenshotURL'].startsWith('data:image/') ){
						mycustomScreenshotURL='","customScreenshotURL":"'+myWWW[i]['customScreenshotURL'];
					}else{
						mycustomScreenshotURL='';
					}
				}else{
					mycustomScreenshotURL='';
				}
				 myWWWList += '{"url":"'+myWWW[i]['url']+'","label":"'+myWWW[i]['label']+mycustomScreenshotURL+'"}'+endStr;
			}else{
				endStr=',null';
				myWWWList += endStr;
			}
		}
	}
	pref("browser.newtabpage.pinned", '['+myWWWList+']');
	pref("my.count_myWWWList", count_myWWWList);
	delete myWWWList;
	delete nLimit;
	delete endStr;
/*************************************************************************/
/** default
 * browser.newtabpage.activity-stream.default.sites;
 * https://www.youtube.com/,
 * https://www.facebook.com/,
 * https://www.wikipedia.org/,
 * https://www.reddit.com/,
 * https://www.amazon.com/,
 * https://twitter.com/
**/
	// myWWW.sort(function(a, b){return a-b}); // reverse
	myWWW.sort();
	myWWWList="";
	for ( i=0 ; i<myWWW.length ; i++ ){
		if ( myWWW[i] != undefined && myWWW[i]["show"] === "true" ){
			myWWWList += myWWW[i]["url"]+",";
		}
	}
	myWWWList=myWWWList.substr(0,myWWWList.length-1); // rimuove l'ultima virgola
	pref("browser.newtabpage.activity-stream.default.sites", ''+myWWWList+'' );
	delete myWWWList;

	/////////////
	pref("browser.onboarding.tour.onboarding-tour-performance.completed", false );
	pref("browser.onboarding.tour.onboarding-tour-library.completed", false );
	pref("browser.onboarding.tour.onboarding-tour-screenshots.completed", false );
	pref("browser.onboarding.tour.onboarding-tour-singlesearch.completed", false );
	pref("browser.onboarding.tour.onboarding-tour-customize.completed", false );
	pref("browser.onboarding.tour.onboarding-tour-sync.completed", false );
	pref("browser.onboarding.tour.onboarding-tour-addons.completed", false );
	pref("browser.onboarding.tour.onboarding-tour-default-browser.completed", false );
	pref("browser.onboarding.tour.onboarding-tour-private-browsing.completed", false );
	pref("browser.onboarding.state", "default" ); //"watermark" ignora la conclusione del tour
	pref("browser.onboarding.enabled", true );
	pref("browser.onboarding.notification.finished", true );
//fine preferenze SCHEDE

//inizio preferenze CONTENUTI
	//blocca popup
	pref("privacy.popups.showBrowserMessage", false);
	pref("privacy.popups.usecustom", false);
	// blocca le finestre pop-up
	pref("dom.disable_open_during_load", true);

	//http://kb.mozillazine.org/Privacy.popups.disable_from_plugins
	pref("privacy.popups.disable_from_plugins", 1);
	pref("dom.popup_maximum", 2); //default 20
	pref("permissions.default.image", 1);

	// resource://app/defaults/preferences/firefox.js
	// Set default fallback values for site permissions we want
	// the user to be able to globally change.
	pref("permissions.default.geo", 0); // default value
	//pref("permissions.default.geo", 2); // Block new requests asking to access your location
	pref("permissions.default.desktop-notification", 0);
	pref("permissions.default.shortcuts", 0);

	pref("permissions.default.camera", 0); // default value
	//pref("permissions.default.camera", 2); // Block new requests asking to access your camera

	pref("permissions.default.microphone", 0); // default value
	//pref("permissions.default.microphone", 2); // Block new requests asking to access your microphone

	//pref("permissions.default.desktop-notification", 0); // default value
	pref("permissions.default.desktop-notification", 2); // Block new requests asking to allow notifications

	pref("browser.display.show_image_placeholders", true);
	pref("pref.advanced.images.disable_button.view_image", true);

	// nelle nuove versioni javascript è sempre abilitato, e non c'è l'impostazione GUI
	// //unlockPref("javascript.enabled");
	// lockPref("javascript.enabled", true);
		pref("dom.disable_window_open_feature.location",  false);
		// lockPref("dom.disable_window_move_resize", false);
		pref("dom.disable_window_move_resize", false);
		// lockPref("dom.disable_window_flip", true);
		pref("dom.disable_window_flip", true);
		// lockPref("dom.event.contextmenu.enabled", true);
		pref("dom.event.contextmenu.enabled", true);
		// lockPref("dom.disable_window_open_feature.status", true);
		pref("dom.disable_window_open_feature.status", true);
		// lockPref("dom.disable_window_status_change", true);
		pref("dom.disable_window_status_change", true);

	// //unlockPref("pref.advanced.javascript.disable_button.advanced");
	// lockPref("pref.advanced.javascript.disable_button.advanced", true);
	pref("pref.advanced.javascript.disable_button.advanced", true);

	// //unlockPref("security.enable_java");
	// lockPref("security.enable_java", true);
	pref("security.enable_java", true);

	// //---INIZIO OCCIDENTALE-----
		pref("font.language.group", "x-western");
		pref("font.name.serif.x-western", "serif");
		pref("font.name.monospace.x-western", "monospace");
		pref("font.name.sans-serif.x-western", "sans-serif");
		pref("font.size.variable.x-western", 14);
		pref("font.minimum-size.x-western", 12);
		pref("font.size.fixed.x-western", 14);
	//---FINE OCCIDENTALE--------

	pref("browser.display.use_document_fonts", 1);
	pref("intl.charset.default", "UTF-8");
	// pref("intl.accept_languages", "it-it,it,en-us,en,en-gb,es,es-es,fr,fr-fr,de,de-de");
	// //defaultPref("intl.accept_languages", "it-it,it,en-us,en,en-gb,es,es-es,fr,fr-fr,de,de-de");
	pref("intl.accept_languages", "it-it,it,en-us,en,en-gb,es,es-es,fr,fr-fr,de,de-de");

	pref("browser.display.use_system_colors", true);
	pref("browser.underline_anchors", true);
	pref("browser.display.use_document_colors", true);
	pref("browser.display.foreground_color", "#000000");
	pref("browser.display.background_color", "#FFFFFF");
	pref("browser.anchor_color", "#3366FF");
	pref("browser.visited_color", "#551A8B");

	//firefox 59
	pref("browser.translation.ui.show", true);
	pref("browser.translation.detectLanguage", false);
	pref("browser.translation.ui.welcomeMessageShown", true);
	clearPref("browser.translation.engine");
	// pref("browser.translation.engine", "bing");
	// pref("browser.translation.neverForLanguages", "it,de,en");
	pref("browser.translation.neverForLanguages", "it");
//fine preferenze CONTENUTI

// inizio plugins impostazioni su ubuntu, per windows verificare.
// 2=attiva sempre
// 1=chiedi prima di attivare
// 0=non attivare
	// vlc
	pref("plugin.state.libtotem-cone-plugin", 2);

	// DivX® Web Player
	pref("plugin.state.libtotem-mully-plugin", 2);

	// google talk
	pref("plugin.state.libnpgoogletalk", 2);
	// google talk video render
	pref("plugin.state.libnpo1d", 2);

	// libreoffice
	pref("plugin.state.libnpsoplugin", 2);

	// flash
	pref("plugin.state.flash", 2);

	// codec video OpenH264
	pref("media.gmp-gmpopenh264.enabled", true);
	pref("media.gmp-gmpopenh264.autoupdate", true);

	// DjView
	//pref("plugin.state.nsdejavu", 1);
	// java
	//pref("plugin.state.java", 1);

	// Windows Media Player Plug-in 10 (compatible; Videos)
	//pref("plugin.state.libtotem-gmp-plugin", 1);

	//iTunes Application Detector
	//pref("plugin.state.librhythmbox-itms-detection-plugin", 1);

	// QuickTime Plug-in
	//pref("plugin.state.libtotem-narrowspace-plugin", 1);
// fine plugins

//inizio preferenze PRIVACY
	// da firefox 66.0.2
	pref("media.autoplay.default", 1);

	// Comunica ai siti visitati la volontà di non essere tracciato
	pref("privacy.donottrackheader.enabled", true);
	// Attiva la protezione antitracciamento in modalità Navigazione anonima
	pref("privacy.trackingprotection.pbmode.enabled", true);

	// Utilizza sempre la modalità Navigazione anonima
	pref("browser.privatebrowsing.autostart", false);

	// //unlockPref("browser.privatebrowsing.dont_prompt_on_enter");
	// lockPref("browser.privatebrowsing.dont_prompt_on_enter", false);
	pref("browser.privatebrowsing.dont_prompt_on_enter", false);
	/**
	 * 0 means remove downloads when they finish downloading
	 *   1 means downloads will be removed when the browser quits
	 *   2 means never remove downloads
	**/pref("browser.download.manager.retention", 1);
	pref("browser.formfill.enable", false);
	// https://developer.mozilla.org/en-US/docs/Mozilla/Cookies_Preferences
	pref("network.cookie.alwaysAcceptSessionCookies", true);
	pref("pref.privacy.disable_button.view_cookies", false);
	pref("pref.privacy.disable_button.cookie_exceptions", true);

	/**
	 * Accetta i cookie di terze parti
	 * network.cookie.cookieBehavior
	 * - determines how the browser should handle cookies:
	 *      0   means enable all cookies
	 *      1   means reject third party cookies; see
	 *          netwerk/cookie/src/nsCookieService.cpp for a hairier definition
	 *      2   means disable all cookies
	 *      3
	 *      4
	 **/pref("network.cookie.cookieBehavior", 1);

	/** network.cookie.lifetimePolicy
	 * - determines how long cookies are stored:
	 *     0   means keep cookies until they expire
	 *     1   means ask how long to keep each cookie
	 *     2   means keep cookies until the browser is closed
	**/	
	pref("network.cookie.lifetimePolicy", 2);
	pref("network.cookie.lifetime.days", 0);
	pref("places.history.enabled", true);
	pref("browser.history_expire_days", 1); //????? Non più supportata
	pref("browser.history_expire_days_min", 1); //??
	/**
	 * la cronologia viene elininata al limite imposto da
	 * pref("browser.history_expire_days", 1); ?????
	**/	
	pref("privacy.clearOnShutdown.history", true);
	pref("privacy.clearOnShutdown.cache", true);
	pref("privacy.clearOnShutdown.cookies", true);
	pref("privacy.clearOnShutdown.downloads", true);
	pref("privacy.clearOnShutdown.formdata", true);
	pref("privacy.clearOnShutdown.offlineApps", true);
	pref("privacy.clearOnShutdown.passwords", true);
	pref("privacy.clearOnShutdown.sessions", true);
	pref("privacy.clearOnShutdown.siteSettings", true);
	pref("privacy.clearOnShutdown.openWindows", true);
	pref("privacy.sanitize.sanitizeOnShutdown", true);
	//-----------------Ctrl-Shift-del
		// lockPref("privacy.cpd.history", true);
		// lockPref("privacy.cpd.cache", true);
		// lockPref("privacy.cpd.cookies", true);
		// lockPref("privacy.cpd.formdata", true);
		// lockPref("privacy.cpd.siteSettings", true);
		// lockPref("privacy.cpd.sessions", true);

		pref("privacy.cpd.history", false);
		pref("privacy.cpd.cache", true);
		pref("privacy.cpd.cookies", true);
		pref("privacy.cpd.formdata", true);
		pref("privacy.cpd.siteSettings", true);
		pref("privacy.cpd.sessions", true);
	//------------------
		// pref("privacy.cpd.passwords", true);
		// pref("privacy.cpd.downloads", true);
		// pref("privacy.cpd.offlineApps", true);
		/**
		 * 0 - Clear everything
		 * 1 - Last Hour
		 * 2 - Last 2 Hours
		 * 3 - Last 4 Hours
		 * 4 - Today
		**/	pref("privacy.sanitize.timeSpan", 0);
//fine preferenze PRIVACY

//preferenze SEARCH
// la prima esecuzione legge queste impostazioni
// da firefox 34 le info sono salvate nel profilo utente nel file "search-metadata.json"

// http://superuser.com/questions/253879/change-the-default-search-engine-for-firefoxs-address-bar
// https://bugzilla.mozilla.org/show_bug.cgi?id=1029148
// https://dxr.mozilla.org/mozilla-central/source/toolkit/components/search/nsSearchService.js#815-838

/*
# !/bin/bash
# esempio
# profile=`tac $HOME/.mozilla/firefox/profiles.ini | sed -n '/^Default=1$/,/^\[Profile[0-9]\]/p' | grep Path= | awk -F"=" '{print $2}'`
profile="4a4aalj8.Default User"
provider=DuckDuckGo
disclaimer="By modifying this file, I agree that I am doing so \
only within Firefox itself, using official, user-driven search \
engine selection processes, and in a way which does not circumvent \
user consent. I acknowledge that any attempt to change this file \
from outside of Firefox is a malicious act, and will be responded \
to accordingly."
hash=`printf "$profile$provider$disclaimer" | openssl sha256 -binary | base64`
*/
// nel file "search-metadata.json"
// {"[global]":{"current":"DuckDuckGo","hash":"Q26RpmnwwwGS8308i0m3nyLFzwvAC9qbnkqvyitJn3s=","searchdefaultexpir":1456062835917}}
// sostituire il valore di hash con quello generato dallo script bash

	// da firefox 40 le info sono salvate nel profilo nel file search-metadata.json

	pref("browser.search.useDBForOrder", false);

	//Mostra i suggerimenti di ricerca prima della cronologia
	//nei risultati della barra degli indirizzi
	pref("browser.urlbar.matchBuckets", "");

	//clearPref("browser.search.geoSpecificDefaults");
	pref("browser.search.geoSpecificDefaults", true);

	//clearPref("browser.search.defaultenginename.US");
	pref("browser.search.defaultenginename.US", "data:text/plain,browser.search.defaultenginename.US=DuckDuckGo");

	//clearPref("browser.search.defaultenginename.IT");
	pref("browser.search.defaultenginename.IT", "data:text/plain,browser.search.defaultenginename.IT=DuckDuckGo");

	//clearPref("distribution.searchplugins.defaultLocale");
	pref("distribution.searchplugins.defaultLocale", "it-IT");

	//clearPref("browser.search.countryCode");
	pref("browser.search.countryCode", "it");

	//clearPref("browser.search.defaultenginename");
	pref("browser.search.defaultenginename", "DuckDuckGo");

	//clearPref("browser.search.selectedEngine");
	pref("browser.search.selectedEngine", "DuckDuckGo");

	//clearPref("browser.search.order.1");
	pref("browser.search.order.1", "DuckDuckGo");

	//clearPref("browser.search.region");
	pref("browser.search.region", "it");

	//clearPref("browser.search.hiddenOneOffs");
	//pref("browser.search.hiddenOneOffs");

	//clearPref("browser.search.geoSpecificDefaults");
	pref("browser.search.geoSpecificDefaults", true);

	//clearPref("browser.search.countryCode");
	pref("browser.search.countryCode", "IT");

	//clearPref("browser.search.defaultenginename");
	pref("browser.search.defaultenginename", "DuckDuckGo");

	//clearPref("browser.search.selectedEngine");
	pref("browser.search.selectedEngine", "DuckDuckGo");

	pref("browser.search.suggest.enabled", true);
	pref("browser.urlbar.suggest.searches", true);
	/**
	 * "browser.urlbar.doubleClickSelectsAll"
	 * false == 2 click seleziona la parola, il terzo
	 * click seleziona tutto in urlbar
	**/	pref("browser.urlbar.doubleClickSelectsAll", false);
	pref("browser.urlbar.usepreloadedtopurls.enabled", true); //?

	/**
	 * in firefox 58-59 - non si imposta???
	 * clearPref("browser.search.widget.inNavBar");
	 * clearPref("browser.uiCustomization.state");
	 * pref("browser.search.widget.inNavBar", false);
	 * dipende da "search-container" in
	 * "browser.uiCustomization.state"
	**/	BROWSER_SEARCH_WIDGET_INNAVBAR=false;
	if (BROWSER_SEARCH_WIDGET_INNAVBAR===true){
		SEARCH_CONTAINER="\"search-container\",";
		pref("browser.search.widget.inNavBar", true);
	} else if (BROWSER_SEARCH_WIDGET_INNAVBAR===false){
		SEARCH_CONTAINER="";
		pref("browser.search.widget.inNavBar", false);
	}
/** impostazione predefinita **/
	//pref("browser.search.widget.inNavBar", true);
	//pref("browser.uiCustomization.state", '{"placements":{"widget-overflow-fixed-list":[],"PersonalToolbar":["personal-bookmarks"],"nav-bar":["back-button","forward-button","stop-reload-button","home-button","urlbar-container","search-container","downloads-button","library-button","sidebar-button"],"TabsToolbar":["tabbrowser-tabs","new-tab-button","alltabs-button"],"toolbar-menubar":["menubar-items"]},"seen":["developer-button"],"dirtyAreaCache":["PersonalToolbar","nav-bar","TabsToolbar","toolbar-menubar"],"currentVersion":14,"newElementCount":0}');
/** ho aggiunto "privatebrowsing-button" **/
	//pref("browser.uiCustomization.state", '{"placements":{"widget-overflow-fixed-list":[],"PersonalToolbar":["personal-bookmarks"],"nav-bar":["back-button","forward-button","stop-reload-button","home-button","urlbar-container",'+SEARCH_CONTAINER+'"privatebrowsing-button","downloads-button","library-button","sidebar-button"],"TabsToolbar":["tabbrowser-tabs","new-tab-button","alltabs-button"],"toolbar-menubar":["menubar-items"]},"seen":["developer-button"],"dirtyAreaCache":["PersonalToolbar","nav-bar","TabsToolbar","toolbar-menubar"],"currentVersion":14,"newElementCount":1}');
/** ho aggiunto "privatebrowsing-button" ho tolto "sidebar-button" **/
	pref("browser.uiCustomization.state",'{"placements":{"widget-overflow-fixed-list":[],"PersonalToolbar":["personal-bookmarks"],"nav-bar":["back-button","forward-button","stop-reload-button","home-button","urlbar-container",'+SEARCH_CONTAINER+'"privatebrowsing-button","downloads-button","library-button"],"TabsToolbar":["tabbrowser-tabs","new-tab-button","alltabs-button"],"toolbar-menubar":["menubar-items"]},"seen":["developer-button"],"dirtyAreaCache":["PersonalToolbar","nav-bar","TabsToolbar","toolbar-menubar"],"currentVersion":14,"newElementCount":2}');
	/////////
//fine preferenze SEARCH

//inizio preferenze SICUREZZA
	pref("xpinstall.whitelist.required", true);
	pref("xpinstall.whitelist.add", "");
	pref("xpinstall.whitelist.add.36", "");
	pref("signon.autofillForms", false);
	pref("signon.expireMasterPassword", true);
	pref("signon.rememberSignons", false);
	pref("browser.safebrowsing.enabled", true);
	pref("browser.safebrowsing.malware.enabled", true);
	pref("browser.safebrowsing.remoteLookups", true);
	pref("pref.privacy.disable_button.view_passwords", true);
	pref("security.ask_for_password", 1);
	//---------------
		pref("security.warn_entering_secure", false);
		pref("security.warn_entering_weak", true);
		pref("security.warn_entering_weak.show_once", false);
		pref("security.warn_leaving_secure", true);
		pref("security.warn_submit_insecure.show_once", false);
		pref("security.warn_submit_insecure", false);
		pref("security.warn_viewing_mixed", true);
		pref("security.warn_viewing_mixed.show_once", false);
	//---------------
//fine preferenze SICUREZZA


//inizio preferenze AVANZATE
/**	Determines which tab in the Privacy section of the preferences is visible.
 *	0 (default): History
 *	1: Saved Forms
 *	2: Passwords
 *	3: Download History
 *	4: Cookies
 *	5: Cache
**/	pref("browser.preferences.advanced.selectedTabIndex", 0); // ormai superata

	//preferenze AVANZATE GENERALE
			pref("accessibility.typeaheadfind.autostart", true);
			pref("accessibility.typeaheadfind", true);
			pref("accessibility.typeaheadfind.flashBar", 1);
			pref("accessibility.browsewithcaret", true);
		//http://kb.mozillazine.org/Layout.spellcheckDefault
			pref("layout.spellcheckDefault", 0);
			pref("spellchecker.dictionary", "UI");
			//pref("spellchecker.dictionary", "it-IT");

			if(getenv("OS")=="Windows_NT"){
				pref("browser.shell.checkDefaultBrowser", true);
				pref("browser.defaultbrowser.notificationbar", true);
			}else{
				pref("browser.shell.checkDefaultBrowser", false);
				pref("browser.defaultbrowser.notificationbar", false);
			}
			// con gnome ~/.config/mimeapps.list imposta il default browser
			///////////
			pref("browser.shell.skipDefaultBrowserCheckOnFirstRun", false);
			pref("browser.shell.defaultBrowserCheckCount", 0);
			pref("browser.shell.didSkipDefaultBrowserCheckOnFirstRun", true);
			pref("pref.general.disable_button.default_browser", false);
			pref("general.autoScroll", true);
			pref("general.smoothScroll", true);
			pref("accessibility.blockautorefresh", false);

	//preferenze AVANZATE NETWORK
		//http://kb.mozillazine.org/Network.proxy.type
		pref("browser.cache.disk.enable", true);
		pref("browser.cache.disk.smart_size.enabled", true);
		// lockPref("browser.cache.disk.capacity", 20480);
		pref("browser.cache.disk.capacity", 51200);
		//pref("browser.cache.disk.capacity", 102400); //100MB
		//pref("browser.cache.disk.capacity", 256000); //default
		//pref("browser.cache.disk.capacity", 512000); //

		pref("browser.offline-apps.notify", true);

		pref("signon.autologin.proxy", true);
		pref("network.proxy.socks_remote_dns", true);



	///////preferenze AVANZATE NETWORK impostazioni proxy start
			/**  http://kb.mozillazine.org/Network.proxy.type
			 *	0 Direct connection, no proxy.
			 *	  (Default in Windows and Mac previous to 1.9.2.4 /Firefox 3.6.4)
			 *	1 Manual proxy configuration.
			 *	2 Proxy auto-configuration (PAC).
			 *	4 Auto-detect proxy settings.
			 *	5 Use system proxy settings.
			 *	  (Default in Linux; default for all platforms,
			 *	  starting in 1.9.2.4 /Firefox 3.6.4)
			**/
			var myproxy=0; //imposto qui la configurazione desiderata

			if(getenv("COMPUTERNAME")===""){
				var WINDOWSNAME="NO__NO";
			}else{
				/** computers in lab arco
				 * cambio il proxy
				**/
				 var WINDOWSNAME=getenv("COMPUTERNAME").toLowerCase();
				 if(WINDOWSNAME.match(/arco[a-z]{0,1}[0-9]{1,2}/ig)){
					var myproxy=0;
					// var myproxy=4;
				}
			}

			var myproxyhost="pfs.lanpareto";
			//var myproxyhost="172.17.0.1";
			var myproxyhostport=3128;

			// pref("network.proxy.no_proxies_on", "localhost, 127.0.0.1, 172.28.*.*, 172.30.*.*");
			// pref("network.proxy.no_proxies_on", "localhost, 127.0.0.1, 172.28.0.0/16, 172.30.0.0/16,");
			pref("network.proxy.no_proxies_on", "localhost, 127.0.0.1, ffcfg.mooo.com, elm.iispareto.eu, wx.iispareto.eu, cfg.iispareto.eu");
			// lockPref("network.proxy.no_proxies_on", "localhost, 127.0.0.*, 172.28.*.*, 172.30.*.*");

			// unlockPref("network.proxy.socks_version");
			// clearPref("network.proxy.socks_version");
			// lockPref("network.proxy.socks_version", 5);
			pref("network.proxy.socks_version", 5);

			/**
			 * https://wiki.mozilla.org/Trusted_Recursive_Resolver
			 * lista DoH - DNS over HTTPS
			 * https://github.com/curl/curl/wiki/DNS-over-HTTPS
			 * Set `network.trr.mode`
			 * 0 is "off by default",
			 * 1 lets Firefox pick whichever is faster,
			 * 2 to make DNS Over HTTPS the browser's first choice but use regular DNS as a fallback
			 * 3 for TRR only mode,
			 * 4 ?????????
			 * 5 to explicitly turn it off
			 * Set `network.trr.uri`. Ones that you may use:
			 * https://mozilla.cloudflare-dns.com/dns-query (Privacy Policy),
			 * https://dns.google.com/experimental
			 **/
			pref("network.trr.mode", 2);
			// pref("network.trr.uri", "https://mozilla.cloudflare-dns.com/dns-query");
			pref("network.trr.uri", "https://doh.libredns.gr/dns-query");
			pref("network.trr.custom_uri", "https://galileo.math.unipd.it/dns-query");
			pref("network.trr.bootstrapAddress", "1.1.1.1");


			if (myproxy==0){ //Direct connection, no proxy.
				pref("network.proxy.type", myproxy);
			}

			if (myproxy==1){ //Manual proxy configuration.
				pref("network.proxy.type", 1);
				pref("network.proxy.share_proxy_settings", false);

				if(getPref("network.proxy.share_proxy_settings")==false){
					pref("network.proxy.http", myproxyhost);
					pref("network.proxy.http_port", myproxyhostport);

					pref("network.proxy.ssl", myproxyhost);
					pref("network.proxy.ssl_port", myproxyhostport);

					pref("network.proxy.ftp", myproxyhost);
					pref("network.proxy.ftp_port", myproxyhostport);

					pref("network.proxy.gopher", myproxyhost);
					pref("network.proxy.gopher_port", myproxyhostport);

					pref("network.proxy.socks", myproxyhost);
					pref("network.proxy.socks_port", myproxyhostport);

				}else{ //"network.proxy.share_proxy_settings",true
					pref("network.proxy.http", myproxyhost);
					pref("network.proxy.http_port", myproxyhostport);
				}
			}

			if (myproxy==2){ //Proxy auto-configuration (PAC).
				pref("network.proxy.type", myproxy);
				pref("network.proxy.autoconfig_url", "http://wpad.lanpareto/wpad.dat");
				//pref("network.proxy.autoconfig_retry_interval_max", 300); //defaut=300
				//pref("network.proxy.autoconfig_retry_interval_min", 5); //defaut=5
			}

			if (myproxy==4){ //Auto-detect proxy settings.
				pref("network.proxy.type", myproxy);
			}

			if (myproxy==5){ //Use system proxy settings.
				pref("network.proxy.type", myproxy);
			}
	///////preferenze AVANZATE NETWORK impostazioni proxy end

	//preferenze AVANZATE AGGIORNAMENTI
		pref("extensions.update.enabled", true);

		pref("browser.search.update", true);

		/**
		 * questo file non è più presente con le nuove installazioni
		 * /usr/lib/firefox-"versions"/defaults/autoconfig/platform.js
		 *
		 * platform.value = "unix"
		**/
		/** solo su windows
		**/
		//if (platform.value == "windows") {
		if(getenv("OS")=="Windows_NT"){
		//if(OS=="Windows_NT"){
			pref("app.update.enabled", true);
			pref("app.update.auto", true);
			pref("app.update.mode", 1); //http://kb.mozillazine.org/App.update.mode
			pref("app.update.service.enabled", true);
		}

		 pref("browser.search.update", true);
	//preferenze AVANZATE AGGIORNAMENTI end

	//preferenze CONDIVISIONE DATI
		pref("datareporting.healthreport.uploadEnabled", true);
		unlockPref("toolkit.telemetry.enabled");
		clearPref("toolkit.telemetry.enabled");
		pref("toolkit.telemetry.enabled", true);
		pref("toolkit.telemetry.reportingpolicy.firstRun", false);
		pref("toolkit.telemetry.rejected", true);
		//pref("toolkit.telemetry.prompted", 2);
		pref("datareporting.policy.dataSubmissionPolicyBypassNotification", true);
		/**
		 * datareporting.policy...... da controllare
		**/
			pref("browser.crashReports.unsubmittedCheck.autoSubmit", true);
			pref("browser.crashReports.unsubmittedCheck.autoSubmit2", true);
			pref("datareporting.healthreport.service.firstRun", false);

			pref("datareporting.policy.dataSubmissionPolicyAcceptedVersion", 2);

	//preferenze AVANZATE CONDIVISIONE DATI fine

	// preferenze AVANZATE CIFRATURA
		pref("security.enable_ssl3", true);
		pref("security.enable_tls", true);
		pref("security.default_personal_cert", "Ask Every Time");
		//pref("security.default_personal_cert", "Select Automatically");

		// http://kb.mozillazine.org/Browser.xul.error_pages.enabled
		pref("browser.xul.error_pages.enabled", true);
		pref("security.OCSP.enabled", 1);
		pref("security.OCSP.require", false);
		pref("security.disable_button.openDeviceManager", false);
//fine preferenze AVANZATE

// view_source
	pref("view_source.wrap_long_lines", true);
	pref("view_source.syntax_highlight", true);
	pref("view_source.editor.external", false);

// }catch(e){
	// /**
	// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/SyntaxError#Examples
	// **/
	// displayError("pref", e);
	// Components.utils.reportError(e);
// }
